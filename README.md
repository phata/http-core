# Phata\HttpCore

[![gitlab-ci-status-img]][gitlab-ci-pipeline]

A very simple and opiniated mechanic to bootstrap a PSR compliant HTTP application.

Compatible to these standards:

1. [PSR-7 HTTP message interfaces][psr-7]
2. [PSR-11: Container interface][psr-11]
3. [PSR-15: HTTP Server Request Handlers][psr-15]
4. [PSR-17: HTTP Server Request Handlers][psr-17]

A container (using DI\Container if not defined) will be bootstrap with all the opinionatly
selected implementations from different libraries. Then the handler may handle the request,
emit the response with the interface implementations.

Example,

```php
<?php

use Phata\HttpCore\Kernel;
use Phata\HttpCore\RouteDefinitionFile;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;

require_once __DIR__ . '/../vendor/autoload.php'; // change this path if needed.

$container = Kernel::bootstrap(new RouteDefinitionFile(__DIR__ . '/routes.php'));
$request = $container->get(ServerRequestInterface::class);
$response = $container->get(RequestHandlerInterface::class)->handle($request);
$container->get(EmitterInterface::class)->emit($response);

```

The bootstraped container, the request, the response can be easily manipulate to your need.
This should provide an easy-to-tinker base for most web application. And can use PSR-15
compliant middleware, or other PSR implementations if needed.


## License

This software is license in MIT license. You can obtain a copy of this license along in
this repository.

[psr-7]: https://www.php-fig.org/psr/psr-7/
[psr-11]: https://www.php-fig.org/psr/psr-11/
[psr-15]: https://www.php-fig.org/psr/psr-15/
[psr-17]: https://www.php-fig.org/psr/psr-17/
[gitlab-ci-pipeline]: https://gitlab.com/phata/http-core/pipelines?scope=branches&page=1
[gitlab-ci-status-img]: https://gitlab.com/phata/http-core/badges/main/pipeline.svg?key_text=main&key_width=56
