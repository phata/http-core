<?php

use Phata\HttpCore\Kernel;
use Phata\HttpCore\RouteDefinitionFile;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;

require_once __DIR__ . '/../vendor/autoload.php';

$container = Kernel::bootstrap(new RouteDefinitionFile(__DIR__ . '/routes.php'));
$request = $container->get(ServerRequestInterface::class);
$response = $container->get(RequestHandlerInterface::class)->handle($request);
$container->get(EmitterInterface::class)->emit($response);
