<?php

use Phata\HttpCore\Route;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;

// The simpliest route is a anoynmous function that echos.
// addRoute accepts either a callable or a Phata\HttpCore\Route.
$r->addRoute('GET', '/', function (ServerRequestInterface $request) {
    echo '<p>Welcome to this example applicaiton!</p>';
    echo '<p>Try these:</p>';
    echo '<ul>';
    echo '<li><a href="/hello">Hello</a></li>';
    echo '<li><a href="/posts">Posts List</a></li>';
    echo '<li><a href="/post/12345/dummy-post">Dummy Post</a></li>';
    echo '</ul>';
});

// The simpliest route is a anoynmous function that echos.
// addRoute accepts either a callable or a Phata\HttpCore\Route.
$r->addRoute('GET', '/hello', function (ServerRequestInterface $request) {
    echo 'hello world: ' . $request->getUri();
});

// An example controller class
class PostController {

    // Method can simply echo. The RequestHandler will create valid psr-7 response from it.
    public function getPosts()
    {
        echo 'hello posts';
    }

    // Method used in route can get the route variable(s)
    // by specifying the name.
    public function getPost($id, $title)
    {
        echo "hello post. id: {$id}, title: {$title}";
    }

    // A route can create its own psr-7 response
    public function getPostRoot(ResponseFactoryInterface $responseFactory)
    {
        // Return a psr-7 ResponseInterface and the RequestHandler will use it.
        return $responseFactory
            ->createResponse(302)
            ->withHeader('Location', '/posts');
    }
}

// A very simple route.
$r->addRoute('GET', '/posts', new Route(PostController::class, 'getPosts'));

// A route with named variables.
// {id} must be a number (\d+)
// The /{title} suffix is optional
$r->addRoute('GET', '/post/{id:\d+}[/{title}]', new Route(PostController::class, 'getPost'));

// A route can create its own psr-7 response
$r->addRoute('GET', '/post', new Route(PostController::class, 'getPostRoot'));
