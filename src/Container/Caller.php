<?php

namespace Phata\HttpCore\Container;

use Phata\HttpCore\Container\CallerInterface;
use Psr\Container\ContainerInterface;

/**
 * A dependency inject caller implementation with the help of
 * to PSR-11 container.
 */
class Caller implements CallerInterface
{

    /**
     * Dependency inject container implementation.
     *
     * @var \Psr\Container\ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param \Psr\Container\ContainerInterface $container
     *     Dependency inject container implementation.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function withContainer(ContainerInterface $container): CallerInterface
    {
        return new Caller($container);
    }

    /**
     * {@inheritDoc}
     */
    public function call(callable $callable, array $vars)
    {
        switch (true) {
            case is_array($callable) && sizeof($callable) > 1:
                $refClass = new \ReflectionClass($callable[0]);
                $params = static::mapParams($this->container, $vars)(
                    ...$refClass->getMethod($callable[1])->getParameters());
                return $callable(...$params);
            case is_a($callable, \Closure::class):
            case function_exists($callable):
                $refFunc = new \ReflectionFunction($callable);
                $params = static::mapParams($this->container, $vars)(
                    ...$refFunc->getParameters());
                return $callable(...$params);
            default:
                throw new \InvalidArgumentException('unexpectable type of callable here');
        }
    }

    /**
     * From a given container and named variable assoc, create a mapper function to
     * convert a ReflectionParameter[] into the appropriate value.
     *
     * @param Psr\Container\ContainerInterface $container
     *     A PSR container containing / knows how to produce class of the given
     *     name or class name.
     * @param array $vars
     *     An array of route variables.
     *
     * @return function (ReflectionParameter ...$refParams): mixed[]
     *     A callback to map ReflectionParameter[] with. Please note you need to use
     *     "..." to spread the array and feed into the callback.
     */
    protected static function mapParams(ContainerInterface $container, array $vars): callable
    {
        return function (\ReflectionParameter ...$refParams) use ($container, $vars) {
            return array_map(function (\ReflectionParameter $refParam) use ($container, $vars) {
                $name = $refParam->getName();
                if ($container->has($name)) {
                    return $container->get($name);
                }
                if ($refParam->hasType()) {
                    $refType = $refParam->getType();
                    switch (get_class($refType)) {
                        case \ReflectionNamedType::class:
                            if ($container->has($refType->getName())) {
                                return $container->get($refType->getName());
                            }
                            throw new \Exception("Container does not have a {$refType->getName()}");
                        default:
                            throw new \InvalidArgumentException("unsupported type " . get_class($refType));
                    }
                    throw new \InvalidArgumentException("unable to provide parameter \"{$name}\" to the route method");
                }

                // if no typehint found, try to find it by variable name
                if (isset($vars[$name])) {
                    return $vars[$name];
                }
            }, $refParams);
       };
    }
}