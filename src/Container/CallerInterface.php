<?php

namespace Phata\HttpCore\Container;

use Psr\Container\ContainerInterface;

/**
 * An abstraction to do dependencies injection on
 * a callable with the help of some key-value pairs.
 */
interface CallerInterface
{
    /**
     * Use another container for calling the callable.
     *
     * @param Psr\Container\ContainerInterface $container
     *
     * @return \Phata\HttpCore\Container\CallerInterface
     */
    public function withContainer(ContainerInterface $container): CallerInterface;

    /**
     * Call a callable with a key-value pair assoc.
     *
     * Should attempt to provide the dependencies needed
     * for the callable to run.
     *
     * @param callable $callable
     *     A PHP callable type.
     * @param array $vars
     *     Associated array of key-value pairs to be used to
     *     execute the callable.
     *
     * @return mixed?
     *     Whatever returned by the callable.
     */
    public function call(callable $callable, array $vars);
}