<?php

namespace Phata\HttpCore\Container;

use Psr\Container\ContainerInterface;

/**
 * A simple container for overriding behaviour of an inner DI container.
 */
class OverrideContainer implements ContainerInterface
{
    /**
     * Inner container.
     *
     * @var \Psr\Container\ContainerInterface
     */
    protected $inner;

    /**
     * Variables for overriding.
     *
     * @var array
     */
    protected $vars;

    /**
     * Class constructor
     *
     * @param ContainerInterface $inner
     *     Container to fallback to if no overriding key-value found.
     *
     * @param array $vars
     *     Variable for overriding.
     */
    public function __construct(ContainerInterface $inner, array $vars = [])
    {
        $this->inner = $inner;
        $this->vars = $vars;
    }

    /**
     * Inject overriding key-values.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return self
     */
    public function with(string $key, $value): self
    {
        $this->vars[$key] = $value;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function get($id)
    {
        return $this->vars[$id] ?? $this->inner->get($id);
    }

    /**
     * {@inheritDoc}
     */
    public function has($id)
    {
        return isset($this->vars[$id]) || $this->inner->has($id);
    }
}