<?php declare(strict_types=1);

namespace Phata\HttpCore;

use DI\Container;
use DI\ContainerBuilder;
use FastRoute\Dispatcher;
use function FastRoute\simpleDispatcher;
use Phata\HttpCore\Container\CallerInterface;
use Phata\HttpCore\Container\Caller;
use Phata\HttpCore\Server\RequestHandler;
use Phata\HttpCore\RouteDefinitionInterface;
use GuzzleHttp\Psr7\ServerRequest;
use Http\Factory\Guzzle\{
    RequestFactory,
    ResponseFactory,
    ServerRequestFactory,
    StreamFactory,
    UploadedFileFactory,
    UriFactory,
};
use Psr\Container\ContainerInterface;
use Psr\Http\Message\{
    ResponseFactoryInterface,
    ResponseInterface,
    ServerRequestFactoryInterface,
    ServerRequestInterface,
    StreamFactoryInterface,
    UploadedFileFactoryInterface,
    UriFactoryInterface,
};
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\HttpHandlerRunner\Emitter\{
    EmitterInterface,
    EmitterStack,
    SapiEmitter,
    SapiStreamEmitter,
};

class Kernel
{
    /**
     * Bootstrap the essential variables for the system.
     *
     * @param \Phata\HttpCore\RouteDefinitionInterface $route_definition
     *     A PHP file containing all the routing instructions.
     * @param \DI\ContainerBuilder|null
     *     An optional preconfigured PHP-DI ContainerBuilder for building
     *     the container. Would create a blank default ContainerBuilder
     *     without configuration.
     *
     * @return \DI\Container
     *     A Psr\Container\ContainerInterface implementation.
     *
     * @throws \InvalidArgumentException
     *     If the $route_file is not a valid filename, throw this error.
     */
    public static function bootstrap(
        RouteDefinitionInterface $route_definition,
        ?ContainerBuilder $container_builder = null
    ): Container
    {
        // Create a DI container builder, if not provided.
        if ($container_builder === null) $container_builder = new ContainerBuilder();

        // setup container with autowirings.
        $container = $container_builder->build();

        // Define default Phata\HttpCore\RouteDefinitionInterface implementation
        // to be a Phata\HttpCore\RouteDefinitionFile of the specific route.
        $container->set(RouteDefinitionInterface::class, $route_definition);

        // Setup router dispatcher
        $container->set(Dispatcher::class, \DI\factory(function (RouteDefinitionInterface $d) {
            return simpleDispatcher([$d, 'collectRoutes']);
        }));

        // Associate the Caller to CallerInterface
        $container->set(CallerInterface::class, \DI\get(Caller::class));

        // Associate the interface that Psr17Factory implements with the "get" helper.
        $container->set(RequestFactoryInterface::class, \DI\get(RequestFactory::class));
        $container->set(ResponseFactoryInterface::class, \DI\get(ResponseFactory::class));
        $container->set(ServerRequestFactoryInterface::class, \DI\get(ServerRequestFactory::class));
        $container->set(StreamFactoryInterface::class, \DI\get(StreamFactory::class));
        $container->set(UploadedFileFactoryInterface::class, \DI\get(UploadedFileFactory::class));
        $container->set(UriFactoryInterface::class, \DI\get(UriFactory::class));

        // Tell container to create Psr\Http\Message\ServerRequestInterface from
        // GuzzleHttp\Psr7\ServerRequest::fromGlobals
        $container->set(ServerRequestInterface::class, \DI\factory([ServerRequest::class, 'fromGlobals']));

        // Associate Psr\Http\Message\ResponseInterface with Psr\Http\Message\ResponseFactoryInterface
        $container->set(ResponseInterface::class, \DI\factory([ResponseFactoryInterface::class, 'createResponse']));

        // Associate Phata\HttpCore\RequestHandler with Psr\Http\Server\RequestHandlerInterface
        $container->set(RequestHandlerInterface::class, \DI\get(RequestHandler::class));

        // Associate custom stack with Laminas\HttpHandlerRunner\Emitter\EmitterInterface;
        $container->set(EmitterInterface::class, \DI\factory(function () {
            $sapiStreamEmitter = new SapiStreamEmitter();
            $conditionalEmitter = new class ($sapiStreamEmitter) implements EmitterInterface {
                private $emitter;

                public function __construct(EmitterInterface $emitter)
                {
                    $this->emitter = $emitter;
                }

                public function emit(ResponseInterface $response) : bool
                {
                    if ($response->hasHeader('Content-Disposition')) {
                        // use stream emitter for content-disposition response
                        return $this->emitter->emit($response);
                    }
                    if ($response->hasHeader('Content-Range')) {
                        // use stream emitter for range response
                        return $this->emitter->emit($response);
                    }
                    if ($response->getBody()->getSize() > 1024 * 1024) {
                        // use stream emitter for large response
                        return $this->emitter->emit($response);
                    }
                    return false;
                }
            };

            $stack = new EmitterStack();
            $stack->push(new SapiEmitter());
            $stack->push($conditionalEmitter);
            return $stack;
        }));

        return $container;
    }

    /**
     * Emit PSR-7 response, if provided
     *
     * @param Psr\Container\ContainerInterface $container
     *     A container to retrieve response emitter.
     * @param Psr\Http\Message\ResponseInterface
     *     An optional HTTP response object for emit.
     */
    public static function emitResponse(ContainerInterface $container, ResponseInterface $response)
    {
        $container->get(EmitterInterface::class)->emit($response);
    }
}
