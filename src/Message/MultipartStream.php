<?php declare(strict_types=1);

namespace Phata\HttpCore\Message;

use Psr\Http\Message\StreamInterface;

class MultipartStream implements StreamInterface
{

    /**
     * An array of parts.
     *
     * @var array[]
     *     An array of assoc with these keys and values:
     *     - headers: a text block of all the headers, without the ending \r\n.
     *     - stream: a StreamInterface implementation.
     */
    protected $parts;

    /**
     * Boundary string for separating the parts.
     *
     * @var string
     */
    protected $boundary;

    /**
     * Tell position. For handling the tell()
     * response.
     *
     * @var int
     */
    protected $tellPos;

    /**
     * Read part pointer. Pointing to the index of the
     * next part to read.
     *
     * @var int
     */
    protected $readPos;

    /**
     * Buffer for read operation. Only for buffering boundaries
     * or part header. Sub-stream contents are never buffered here.
     *
     * @var string
     */
    private $readBuffer;

    /**
     * Tracking reading states.
     *
     * @var int
     */
    private $readState;

    /**
     * The reading state of the read() method.
     */
    protected const READING_NOTHING = 0;
    protected const READING_PART_START_BOUNDARY = 1;
    protected const READING_PART_HEADERS = 2;
    protected const READING_PART_CONTENTS = 3;
    protected const READING_PART_END_BOUNDARY = 4;

    /**
     * Specify the read more of the internal
     * method readWithPolicy().
     */
    protected const READ_POLICY_READ = 1;
    protected const READ_POLICY_GET_CONTENTS = 2;

    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->parts = [];
        $this->boundary = 'boundary.' . uniqid('', true);
        $this->readBuffer = '';
        $this->readPos = 0;
        $this->readState = self::READING_NOTHING;
    }

    /**
     * Add a stream to the multipart stream
     *
     * @param StreamInterface $stream
     * @param array $headers
     *
     * @return self
     */
    public function add(StreamInterface $stream, array $headers = []): self
    {
        $this->parts[] = [
            'headers' => trim(implode("\r\n", $headers), " \r\n"),
            'stream' => $stream,
        ];
        return $this;
    }

    /**
     * Get undocumented variable
     *
     * @return string
     */
    public function getBoundary()
    {
        return $this->boundary;
    }

    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * This method MUST attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.
     *
     * Warning: This could attempt to load a large amount of data into memory.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString()
    {
        if ($this->isSeekable()) {
            $this->seek(0);
        }
        if ($this->isReadable()) {
            $contents = '';
            while (!$this->eof()) {
                $contents .= $this->read(4096);
            }
            return $contents;
        }
        // for non-readable stream, call getContents
        return $this->getContents();
    }

    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     */
    public function close() {
        foreach (array_keys($this->parts) as $i) {
            $this->parts[$i]['stream']->close();
        }
    }

    /**
     * Separates any underlying resources from the stream.
     *
     * After the stream has been detached, the stream is in an unusable state.
     *
     * @return resource|null Underlying PHP stream, if any
     */
    public function detach()
    {
        foreach (array_keys($this->parts) as $i) {
            $this->parts[$i]['stream']->detach();
        }
        // Note: I can't think of a way to implement the return
        // value. Will return null for the moment.
        return null;
    }

    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize()
    {
        // Must have an end boundary. Add to size.
        $size = strlen($this->renderPartEnd());

        // Each part will have certain overhead content.
        $overheader = $this->calcOverheadSize();
        foreach (array_keys($this->parts) as $i) {
            $size +=
                $this->parts[$i]['stream']->getSize() +
                strlen($this->parts[$i]['headers']) +
                $overheader;
        }
        return $size;
    }

    /**
     * Returns the current position of the file read/write pointer
     *
     * @return int Position of the file pointer
     * @throws \RuntimeException on error.
     */
    public function tell()
    {
        return $this->tellPos;
    }

    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     */
    public function eof()
    {
        // check of any of the parts is not eof.
        foreach (array_keys($this->parts) as $i) {
            if (!$this->parts[$i]['stream']->eof()) return false;
        }
        return $this->bufferEmpty();
    }

    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable()
    {
        // I don't think a multipart stream needs to be
        // seekable at all.
        return false;
    }

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $offset Stream offset
     * @param int $whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws \RuntimeException on failure.
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        // Note: doesn't seem to matter because the
        // stream is not supposed to be seekable.
        throw new \RuntimeException('A multipart steam is not seekable');
    }

    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     * @throws \RuntimeException on failure.
     */
    public function rewind()
    {
        // Note: doesn't seem to matter because the
        // stream is not supposed to be seekable.
        // Simply future proof.
        if (!$this->isSeekable()) {
            throw new \RuntimeException('A multipart steam is not seekable');
        }
        $this->seek(0);
    }

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable()
    {
        return false;
    }

    /**
     * Write data to the stream.
     *
     * @param string $string The string that is to be written.
     * @return int Returns the number of bytes written to the stream.
     * @throws \RuntimeException on failure.
     */
    public function write($string)
    {
        // Note: doesn't seem to matter because the
        // stream is not supposed to be seekable.
        throw new \RuntimeException('A multipart steam is not writable');
    }

    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable()
    {
        // check of any of the parts is not readable.
        foreach (array_keys($this->parts) as $i) {
            if (!$this->parts[$i]['stream']->isReadable()) return false;
        }
        return true;
    }

    /**
     * Read data from the stream.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws \RuntimeException if an error occurs.
     */
    public function read($length)
    {
        return $this->readWithPolicy($length, self::READ_POLICY_READ);
    }


    /**
     * Returns the remaining contents in a string
     *
     * @return string
     * @throws \RuntimeException if unable to read or an error occurs while
     *     reading.
     */
    public function getContents()
    {
        $contents = '';
        while (!$this->eof()) {
            $contents .= $this->readWithPolicy(4096, self::READ_POLICY_GET_CONTENTS);
        }
        return $contents;
    }

    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string $key Specific metadata to retrieve.
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     */
    public function getMetadata($key = null)
    {
        switch ($key) {
            case 'eof':
                return $this->eof(); // different from php://temp, which has no eof
            case 'unread_bytes':
                return $this->getSize() - $this->tell();
            case 'stream_type':
                return 'PHP'; // emulates a php://temp stream
            case 'wrapper_type':
                return 'TEMP'; // emulates a php://temp stream
            case 'mode':
                return 'rb'; // always read-only
            case 'seekable':
                return $this->isSeekable();
            case 'uri':
                return 'php://temp'; // emulates a php://temp stream
            default:
                // if the provided key is not found, return null
                if ($key !== null) return null;
        }

        // if no key is provided ($key === null)
        return [
            'eof' => $this->getMetadata('eof'),
            'unread_bytes' => $this->getMetadata('unread_bytes'),
            'stream_type' => $this->getMetadata('stream_type'),
            'wrapper_type' => $this->getMetadata('wrapper_type'),
            'mode' => $this->getMetadata('mode'),
            'seekable' => $this->getMetadata('seekable'),
            'uri' => $this->getMetadata('uri'),
        ];
    }

    /**
     * Internal implementation of both read and getContents.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @param integer $policy
     *     Policy to read contents. Either READ_POLICY_READ or
     *     READ_POLICY_GET_CONTENTS.
     *
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws \RuntimeException if an error occurs.
     */
    protected function readWithPolicy(int $length, int $policy)
    {
        $contents = '';
        $remains = $length;

        while ($remains > 0) {

            // clear whatever is in the buffer first.
            $remains -= $this->readFromBuffer($remains, $contents);
            if ($remains <= 0) break; // break out of while loop

            // check if the readPos is out of bound
            if (!isset($this->parts[$this->readPos])) {
                // Note: Out of bound.
                $this->appendBuffer($this->renderPartEnd());
                $remains -= $this->readFromBuffer($remains, $contents);
                break; // break out of while loop
            }

            switch ($this->readState) {
                case self::READING_NOTHING:
                    $this->readState = self::READING_PART_START_BOUNDARY;
                    // Put beginning boundary into buffer and read there.
                    // Note: unread part will be in the buffer for further read.
                    $this->appendBuffer($this->renderPartBeginning());
                    $remains -= $this->readFromBuffer($remains, $contents);
                    break; // break out of switch
                case self::READING_PART_START_BOUNDARY:
                    $this->readState = self::READING_PART_HEADERS;
                    if (!empty($this->parts[$this->readPos]['headers'])) {
                        // Put the part header into buffer, then read there.
                        // Note: unread part will be in the buffer for further read.
                        $this->appendBuffer($this->parts[$this->readPos]['headers'] . "\r\n");
                    }
                    // Put EOL here because the header is ended, even if there
                    // is none.
                    $this->appendBuffer("\r\n");
                    $remains -= $this->readFromBuffer($remains, $contents);
                    break; // break out of switch
                case self::READING_PART_HEADERS:
                    $this->readState = self::READING_PART_CONTENTS;
                    $remains -= $this->readFromStream($this->parts[$this->readPos]['stream'], $remains, $contents);
                    break; // break out of switch
                case self::READING_PART_CONTENTS:
                    // Check remaining size in the curreend boundary into buffer, then read there.
                    // Note: unread part will be in the buffer for further read.nt stream.
                    // If something left, read there.
                    if ($policy === self::READ_POLICY_READ) {
                        // Use underlying read method to read contents
                        $current = &$this->parts[$this->readPos]['stream'];
                        $remains -= $this->readFromStream($current, $remains, $contents);
                        if ($current->eof()) $this->readState = self::READING_PART_END_BOUNDARY;
                    } elseif ($policy === self::READ_POLICY_GET_CONTENTS) {
                        // Use getContents to read stream contents. Important
                        // for streams that are not "readable".
                        $current = &$this->parts[$this->readPos]['stream'];
                        $this->appendBuffer($current->getContents());
                        $remains -= $this->readFromBuffer($remains, $contents);
                        $this->readState = self::READING_PART_END_BOUNDARY;
                    }
                    break; // break out of switch
                case self::READING_PART_END_BOUNDARY:
                    // prepare for reading next part
                    $this->readState = self::READING_NOTHING;
                    $this->readPos += 1;

                    // Put the part end here.
                    $this->appendBuffer("\r\n");
                    $remains -= $this->readFromBuffer($remains, $contents);
                    break;
            }
        }

        // Update tell position
        $this->tellPos += $length - $remains;
        return $contents;
    }

    /**
     * Put contents into buffer for use in readFromBuffer()
     *
     * @param string $contents
     *     Contents to put into buffer.
     * @return void
     */
    protected function appendBuffer(string $contents)
    {
        $this->readBuffer .= $contents;
    }

    /**
     * Check if read buffer is empty.
     *
     * @return bool
     */
    protected function bufferEmpty(): bool
    {
        return empty($this->readBuffer);
    }

    /**
     * Read buffered contents into a give $contents string
     *
     * @param int $length
     *     Length of content attempting to read.
     * @param string &$contents
     *     A string, passed by reference, to receive the reading.
     *
     * @return integer
     *     Actual number of bytes read from the buffer.
     */
    protected function readFromBuffer(int $length, string &$contents): int
    {
        $part_contents = substr($this->readBuffer, 0, $length);
        $this->readBuffer = substr($this->readBuffer, $length) ?: '';
        $contents .= $part_contents;
        return strlen($part_contents);
    }

    /**
     * Read buffered contents into a give $contents string
     *
     * @param \Psr\Http\Message\StreamInterface $stream
     *     Stream to read from.
     * @param int $length
     *     Length of content attempting to read from the stream.
     * @param string &$contents
     *     A string, passed by reference, to receive the reading.
     *
     * @return integer
     *     Actual number of bytes read from the stream.
     */
    protected function readFromStream(
        StreamInterface $stream,
        int $length,
        string &$contents
    ): int
    {
        $part_contents = $stream->read($length);
        $contents .= $part_contents;
        return strlen($part_contents);
    }

    /**
     * Render the starting boundary
     *
     * @return string
     *     Starting boundary string.
     */
    protected function renderPartBeginning(): string
    {
        return '--' . $this->getBoundary() . "\r\n";
    }

    /**
     * Render the ending boundary
     *
     * @return string
     *     Ending boundary string.
     */
    protected function renderPartEnd(): string
    {
        return '--' . $this->getBoundary() . "--\r\n";
    }

    /**
     * Calculates the overhead size per part
     * (except header size, which differs every part)
     *
     * @return integer
     *     Byte size of the overhead per part.
     */
    protected function calcOverheadSize(): int
    {
        return strlen(
            // part start boundary
            $this->renderPartBeginning() .
            // header ending
            "\r\n\r\n" .
            // content ending
            "\r\n"
        );
    }
}