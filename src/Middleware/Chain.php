<?php declare(strict_types=1);

namespace Phata\HttpCore\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Chain implements MiddlewareInterface
{
    /**
     * @var Psr\Http\Server\MiddlewareInterface
     */
    protected $middlewares;

    /**
     * Constructor
     *
     * @param \Psr\Http\Server\MiddlewareInterface ...$middlewares
     *     PSR-15 middlewares to chain with.
     *
     * @throws \InvalidArgumentException
     *     If no middleware is provided to this function.
     */
    public function __construct(MiddlewareInterface ...$middlewares)
    {
        $this->middlewares = $middlewares;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $len = sizeof($this->middlewares);
        if ($len <= 0) {
            throw new \InvalidArgumentException('Need to provide at least one PSR-15 middleware.');
        }

        // Assemble the stack at last minute.
        //
        // Note: The innermost layer has the handler, which comes last
        // so there is no way to build the middleware stack before
        // processing. (Unless implementing an extra placeholder that
        // pending a side-effect to set it up somehow. But depending
        // on side-effect is not a very good practice)
        $stack = new Layer($this->middlewares[$len-1], $handler);
        for ($i=$len-2; $i>=0; $i--) {
            $stack = new Layer($this->middlewares[$i], $stack);
        }

        // Handle the request.
        return $stack->handle($request);
    }
}