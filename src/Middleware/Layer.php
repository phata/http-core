<?php declare(strict_types=1);

namespace Phata\HttpCore\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Layer implements RequestHandlerInterface
{
    /**
     * @var Psr\Http\Server\MiddlewareInterface
     */
    protected $middleware;

    /**
     * @var Psr\Http\Server\RequestHandlerInterface
     */
    protected $handler;

    /**
     * Constructor
     *
     * @param \Psr\Http\Server\MiddlewareInterface $middleware
     *     The middleware to wrap with
     */
    public function __construct(MiddlewareInterface $middleware, RequestHandlerInterface $handler)
    {
        $this->middleware = $middleware;
        $this->handler = $handler;
    }

    /**
     * {@inheritDoc}
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->middleware->process($request, $this->handler);
    }
}