<?php declare(strict_types=1);

namespace Phata\HttpCore;

use Psr\Container\ContainerInterface;

/**
 * Route wrapper for lazyloading a class object to produce a callable
 * from the specified classmethod of it.
 *
 * This wrapper do not attempt verify the existance of the id nor the
 * method of it when declared. It will attempt to c
 *
 * @category class
 */
class Route
{
    protected $id;
    protected $methodName;

    /**
     * Constructor.
     *
     * @param string $id
     *      The ID for obtaining, supposedly, an object.
     * @param string $methodName
     */
    public function __construct(string $id, string $methodName)
    {
        $this->id = $id;
        $this->methodName = $methodName;
    }

    /**
     * Get the id string for loading object from the container.
     *
     * @return string
     *      ID to be supplied to Psr\Container\ContainerInterface::get().
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get the specified method name. Assume that the object of said
     * ID will provide this method.
     *
     * @return string
     *      Method name.
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }

    /**
     * Retrieve the object of the specified ID.
     *
     * @param ContainerInterface $container
     * @return callable
     */
    public function createCallable(ContainerInterface $container): callable
    {
        $id = $this->getId();
        $methodName = $this->getMethodName();

        // check if the container has the id
        if (!$container->has($id)) {
            throw new \Exception("unable to find get {$id} in the provided container.");
        }

        // If the class exists, do a static method check.
        if (class_exists($id)) {
            // If the id is a class and the method is a static method of it,
            // produce a callable of that static method. This would avoid
            // producing an actual object of the class.
            $refClass = new \ReflectionClass($id);
            if ($refClass->hasMethod($methodName) && $refClass->getMethod($methodName)->isStatic()) {
                return [$id, $methodName];
            }
        }

        // Seems no way to prevent getting the supposedly object from the container.
        $object = $container->get($id);

        // check if it is an object
        if (!is_object($object)) {
            $type = gettype($object);
            throw new \Exception("the value of {$id} is {$type} instead of object.");
        }

        // check if the method exists
        if (!method_exists($object, $methodName)) {
            // if the method do not exists at all.
            throw new \Exception("the object {$id} does not have a method {$methodName}.");
        }

        // check if the method is static
        $refClass = new \ReflectionClass($object);
        if ($refClass->getMethod($methodName)->isStatic()) {
            // be nice if it is a static method.
            return [$refClass->getName(), $methodName];
        }

        // the simpliest form of callable
        return [$object, $methodName];
    }
}
