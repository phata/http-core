<?php

namespace Phata\HttpCore;

use FastRoute\RouteCollector;
use Phata\HttpCore\RouteDefinitionInterface;

/**
 * Represents a route definition file. For lazy-loading route
 * definitions.
 */
class RouteDefinitionFile implements RouteDefinitionInterface
{

    /**
     * Path to the route file
     *
     * @var string
     */
    protected $filepath;

    /**
     * Constructor
     *
     * @param string $route_file
     *      Full path to a route definition file.
     */
    public function __construct(string $route_file)
    {
        if (!is_file($route_file)) {
            throw new \InvalidArgumentException("route file {$route_file} not found");
        }
        $this->filepath = $route_file;
    }

    /**
     * Implements Phata\HttpCore\RouteDefinitionInterface::collectRoutes
     *
     * @param RouteCollector $r
     *     The route collector provided.
     *
     * @return void
     */
    public function collectRoutes(RouteCollector $r): void
    {
        ob_start();
        require_once $this->filepath;
        $content = ob_get_clean();
        if (!empty($content)) {
            throw new \Exception('Unexpected content from route file: ' . $content);
        }
    }
}