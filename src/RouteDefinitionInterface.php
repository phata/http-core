<?php

namespace Phata\HttpCore;

use FastRoute\RouteCollector;

/**
 * A route definition interface for collecting routes.
 */
interface RouteDefinitionInterface
{
    /**
     * Modify the RouteCollector with its route definition details.
     *
     * @param FastRoute\RouteCollector $r
     *     Route collector from the FastRoute package.
     */
    public function collectRoutes(RouteCollector $r): void;
}