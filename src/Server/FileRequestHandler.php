<?php declare(strict_types=1);

namespace Phata\HttpCore\Server;

use GuzzleHttp\Psr7\LimitStream;
use Phata\HttpCore\Message\MultipartStream;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * A simple request handler for handling file asset response.
 * Knows how to deal with ranged request with partial content
 * response.
 *
 * <code>
 * // If your server request resolves to a specific filesystem $path
 * // of static asset.
 * $path = SomeController::parseRealPath($serverRequest);
 * $streamFactory = $container->get(\Psr\Http\Message\StreamFactoryInterface::class);
 * $responseFactory = $container->get(\Psr\Http\Message\ResponseFactoryInterface::class);
 *
 * // You can then handle the response like so.
 * $handler = new \Phata\HttpCore\Server\FileRequestHandler($streamFactory, $responseFactory, $path);
 * </code>
 */
class FileRequestHandler implements RequestHandlerInterface
{
    /**
     * @var \Psr\Http\Message\StreamFactoryInterface
     */
    protected $streamFactory;

    /**
     * @var \Psr\Http\Message\ResponseFactoryInterface
     */
    protected $responseFactory;

    /**
     * Path to the file.
     *
     * @var string
     */
    protected $path;

    /**
     * Constructor
     *
     * @param string $path
     *     Path to the file.
     *
     * @return \InvalidArgumentException
     *     If the provided path is not of a file.
     */
    public function __construct(
        StreamFactoryInterface $streamFactory,
        ResponseFactoryInterface $responseFactory,
        string $path
    ) {
        if (!is_file($path)) {
            throw new \InvalidArgumentException("Not a file: {$path}");
        }
        $this->path = $path;
        $this->streamFactory = $streamFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Parse the request header "Range" value
     * into an array of ranges, each an assoc
     * array with key-values:
     *
     * @param string $range_header
     *     The Range http header value.
     *     References:
     *     https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range
     *     https://tools.ietf.org/html/rfc7233#section-3.1
     * @param int $size_limit
     *     The size limit to the specific request.
     *     Usually the actual size limit of the underlying resource.
     * @param string[] $accepted_units
     *     An optional accepted units array.
     *     By default, ['bytes'].
     *
     * @return array
     *     An array of key-values assoc, each with these keys:
     *      - unit: The unit of values.
     *      - start: The start position.
     *      - end: The end position.
     *      - size: The size of the chunk.
     *      - total: The size of the whole content.
     *
     * @throws \Exception
     */
    public static function parseRequestRange(
        string $range_header,
        int $size_limit,
        array $accepted_units = ['bytes']
    ): array
    {
        if ($range_header === '') {
            return [];
        }
        if (!preg_match('/^(?P<unit>.+?)\=(?P<values>.+)$/', $range_header, $matches)) {
            // TODO: make a specific exception for this
            throw new \Exception('Invalid other-ranges-specifier format: ' . $range_header);
        }

        ['unit' => $unit, 'values' => $values] = $matches;
        if (!in_array($unit, $accepted_units)) {
            // TODO: make a specific exception for this
            throw new \Exception('Unacceptable range unit: ' . $unit);
        }

        return array_map(function ($value) use ($unit, $size_limit) {
            $value = trim($value);
            if (!preg_match('/^(?P<start>\d+|)\-(?P<end>\d+|)$/', $value, $matches)) {
                // TODO: make a specific exception for this
                throw new \Exception('Invalid other-range-set format: ' . $value);
            }
            $matches['start'] = ((int) $matches['start'] ?: 0);
            $matches['end'] = (int) ($matches['end'] ?: ($size_limit - 1));
            if ($matches['end'] < $matches['start']) {
                // TODO: make a specific exception for this
                throw new \Exception('Invalid range: end is smaller than start');
            }
            return [
                'unit' => $unit,
                'start' => $matches['start'],
                'end' => $matches['end'],
                // Note: size includes the byte at end position
                'size' => 1 + $matches['end'] - $matches['start'],
                'total' => $size_limit,
            ];
        }, explode(',', $values));
    }

    /**
     * Implements Psr\Http\Server\RequestHandlerInterface::handle
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     *     The server request to handle.
     *
     * @return Psr\Http\Message\ResponseInterface
     *     The response to be emit.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $contentType = static::getMimeContentType($this->path);
        $fileSize = filesize($this->path);
        $filemtime = filemtime($this->path);
        $etag = '"' . sha1(basename($this->path) . $filemtime) . '"';

        // if etag is the same as before, not modified.
        // TODO: handle multiple etag (comma separated)
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests
        if ($request->getHeaderLine('If-None-Match') === $etag) {
            return $this->responseFactory->createResponse(304);
        }

        // handle range request
        if (($range_header = $request->getHeaderLine('Range')) !== '') {
            $parsedRanges = static::parseRequestRange($range_header, $fileSize);

            // if there is only a single range
            if (sizeof($parsedRanges) === 1) {
                $range = $parsedRanges[0];
                // check if the range fulfilled.
                if ($range['size'] < 0) {
                    // Give proper error response
                    // Request range not staisfiable
                    return $this->responseFactory->createResponse(416)
                        ->withAddedHeader('Content-Type', $contentType)
                        ->withAddedHeader('Accept-Ranges', 'bytes')
                        ->withAddedHeader('Content-Length', 0)
                        ->withAddedHeader('Content-Range', "{$range['unit']} */{$fileSize}")
                        ->withAddedHeader('Cache-Control', 'max-age=86400')
                        ->withAddedHeader('ETag', $etag);
                }
                return $this->responseFactory->createResponse(206, 'Partial Content')
                    ->withAddedHeader('Content-Type', $contentType)
                    ->withAddedHeader('Accept-Ranges', 'bytes')
                    ->withAddedHeader('Content-Length', $range['size'])
                    ->withAddedHeader('Content-Range', "{$range['unit']} {$range['start']}-{$range['end']}/{$range['total']}")
                    ->withAddedHeader('Cache-Control', 'max-age=86400')
                    ->withAddedHeader('ETag', $etag)
                    ->withBody($this->streamFactory->createStreamFromFile($this->path));
            }

            // Support returning multi-part document.
            $stream = new MultipartStream();
            foreach ($parsedRanges as $range) {
                $substream = new LimitStream(
                    $this->streamFactory->createStreamFromFile($this->path),
                    $range['size'], $range['start']);
                $stream->add($substream, [
                    'Content-Type: ' . $contentType,
                    'Content-Length: ' . $range['size'],
                    'Content-Range: ' . "{$range['unit']} {$range['start']}-{$range['end']}/{$range['total']}",
                ]);
            }
            return $this->responseFactory->createResponse(206, 'Partial Content')
                ->withAddedHeader('Content-Type', 'multipart/byteranges; boundary=' . $stream->getBoundary())
                ->withAddedHeader('Content-Length', $fileSize)
                ->withAddedHeader('Cache-Control', 'max-age=86400')
                ->withAddedHeader('ETag', $etag)
                ->withBody($stream);
        }

        // basic response
        $response = $this->responseFactory->createResponse(200)
            ->withAddedHeader('Content-Type', $contentType)
            ->withAddedHeader('Accept-Ranges', 'bytes')
            ->withAddedHeader('Content-Length', $fileSize)
            ->withAddedHeader('Cache-Control', 'max-age=86400')
            ->withAddedHeader('ETag', $etag);
        return $response->withBody(
            $this->streamFactory->createStreamFromFile($this->path));
    }

    /**
     * Get mime type of the given filename.
     *
     * @reference https://www.php.net/manual/en/function.mime-content-type.php#87856
     *
     * @param string $filename Full path to the file to check.
     *
     * @return string MIME content type.
     */
    public static function getMimeContentType(string $filename): string
    {

        $mime_types = [

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'mp4' => 'video/mp4',
            'mkv' => 'video/webm',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        ];

        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
	}
	if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        return 'application/octet-stream';
    }
}
