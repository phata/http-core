<?php declare(strict_types=1);

namespace Phata\HttpCore\Server;

use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Factory to create file request handler.
 * For using DI container with.
 *
 * <code>
 * // If your server request resolves to a specific filesystem $path
 * // of static asset.
 * $path = SomeController::parseRealPath($serverRequest);
 *
 * // Supposed your DI container already has StreamFactory, ResponseFactory
 * // and knows how to produce FileRequestHandlerFactory from them.
 * $factory = $container->get(\Phata\HttpCore\Server\FileRequestHandlerFactory::class);
 * $handler = $factory->createRequestHandler($path);
 * $handler->handle($serverRequest);
 * </code>
 */
class FileRequestHandlerFactory {

    /**
     * @var \Psr\Http\Message\StreamFactoryInterface
     */
    protected $streamFactory;

    /**
     * @var \Psr\Http\Message\ResponseFactoryInterface
     */
    protected $responseFactory;

    /**
     * Constructor
     *
     * @param \Psr\Http\Message\StreamFactoryInterface $streamFactory
     * @param \Psr\Http\Message\ResponseFactoryInterface $responseFactory
     */
    public function __construct(
        StreamFactoryInterface $streamFactory,
        ResponseFactoryInterface $responseFactory
    ) {
        $this->streamFactory = $streamFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Get a file request handler.
     *
     * @param string $path
     * @return RequestHandlerInterface
     */
    public function createRequestHandler(string $path): RequestHandlerInterface
    {
        return new FileRequestHandler($this->streamFactory, $this->responseFactory, $path);
    }
}
