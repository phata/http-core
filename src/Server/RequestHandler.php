<?php declare(strict_types=1);

namespace Phata\HttpCore\Server;

use FastRoute\Dispatcher;
use Phata\HttpCore\Container\CallerInterface;
use Phata\HttpCore\Container\OverrideContainer;
use Phata\HttpCore\Route;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RequestHandler implements RequestHandlerInterface
{
    /**
     * Dependency inject container
     *
     * @var \Psr\Container\ContainerInterface
     */
    protected $container;

    /**
     * Class contstructor
     *
     * @param Psr\Container\ContainerInterface $container
     *     A container for Route::createCallable to work, if needed.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Implements Psr\Http\Server\RequestHandlerInterface::handle
     *
     * @param Psr\Http\Message\ServerRequestInterface $request
     *     The server request to handle.
     *
     * @return Psr\Http\Message\ResponseInterface
     *     The response to be emit.
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // retrieve the relevant interfaces at the begining of request handling process.

        /**
         * @var \FastRoute\Dispatcher
         */
        $dispatcher = $this->container->get(Dispatcher::class);

        /**
         * @var \Psr\Http\Message\ResponseFactoryInterface
         */
        $responseFactory = $this->container->get(ResponseFactoryInterface::class);

        /**
         * @var \Psr\Http\Message\StreamFactoryInterface
         */
        $streamFactory = $this->container->get(StreamFactoryInterface::class);

        // find route for the request.
        $routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getUri()->getPath());
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                // ... 404 Not Found
                $body = $streamFactory->createStream('Not Found');
                return $responseFactory->createResponse(404)->withBody($body);
            case Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1]; // An array of allowed HTTP methods.
                // ... 405 Method Not Allowed
                $body = $streamFactory->createStream('Method Not Allowed.');
                return $responseFactory
                    ->createResponse(405)
                    ->withHeader('Allow', implode(', ', $allowedMethods))
                    ->withBody($body);
            case Dispatcher::FOUND:
                $route = $routeInfo[1];
                $vars = $routeInfo[2];
                $callable = null;

                switch (true) {
                    case $route instanceof Route:
                        $callable = $route->createCallable($this->container);
                    break;
                    case is_callable($route):
                        $callable = $route;
                    break;
                    default:
                        $type = is_object($route) ? get_class($route) : gettype($route);
                        // expect some error handler will handle this.
                        throw new \Exception('Unsupported route type: ' . $type);
                    break;
                }

                /**
                 * @var \Phata\HttpCore\Container\CallerInterface
                 */
                $caller = $this->container->get(CallerInterface::class);

                ob_start();
                $response = $caller
                    ->withContainer(new OverrideContainer($this->container, [
                        ServerRequestInterface::class => $request,
                    ]))
                    ->call($callable, $vars);
                $stdout = ob_get_clean();
                if ($response instanceof ResponseInterface) {
                    return $response;
                }

                // If there is stdout, add "\n" to the end to separate from
                // other outputs.
                if (!empty($stdout)) {
                    $stdout .= "\n";
                }

                // force using the response as string, then response with it
                // (prepended with STDOUT)
                $body = $streamFactory->createStream($stdout . $response);
                return $responseFactory->createResponse(200)->withBody($body);
        }
        $body = $streamFactory->createStream('Internal server error');
        return $responseFactory->createResponse(400)->withBody($body);
    }
}
