<?php

namespace Phata\HttpCore\Container\Test;

use DI\Container;
use Phata\HttpCore\Container\Caller;

class StringHolder {
    function __construct(string $msg)
    {
        $this->msg = $msg;
    }

    function __toString(): string
    {
        return $this->msg;
    }

    function getMessage(): string
    {
        return $this->msg;
    }
}

class Dummy1 extends StringHolder{}
class Dummy2 extends StringHolder{}
class Dummy3 extends StringHolder{}

function getDummy(Dummy1 $dummy1) {
    return $dummy1;
}

class CallerTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /** @specify */
    protected $secret;

    protected function _before()
    {
        $this->secret = rand(0, 100000);
    }

    protected function _after()
    {
    }

    // tests
    public function testMapParamsByName()
    {
        $container = new Container();
        $container->set('foo', 'foo' . $this->secret);
        $container->set('bar', 'bar' . $this->secret);
        $callable = function ($foo, $bar, $hello) {
            return [
                'foo' => $foo,
                'bar' => $bar,
                'hello' => $hello,
            ];
        };
        $caller = new Caller($container);
        $result = $caller->call($callable, [
            'hello' => 'hello' . $this->secret,
        ]);

        $this->assertEquals([
            'foo' => 'foo' . $this->secret,
            'bar' => 'bar' . $this->secret,
            'hello' => 'hello' . $this->secret,
        ], $result, 'Expect to get all named variables.');
    }

    // tests
    public function testMapParamsByClassAndName()
    {
        $container = new Container();
        $container->set(Dummy1::class, new Dummy1('dummy1_' . $this->secret));
        $container->set(Dummy2::class, new Dummy2('dummy2_' . $this->secret));
        $callable = function (Dummy1 $foo, Dummy2 $bar, $hello) {
            return [
                'foo' => (string) $foo,
                'bar' => (string) $bar,
                'hello' => (string) $hello,
            ];
        };
        $caller = new Caller($container);
        $result = $caller->call($callable, [
            'foo' => 'foo' . $this->secret, // should have been ignored
            'bar' => 'bar' . $this->secret, // should have been ignored
            'hello' => 'hello' . $this->secret,
        ]);

        $this->assertEquals([
            'foo' => 'dummy1_' . $this->secret,
            'bar' => 'dummy2_' . $this->secret,
            'hello' => 'hello' . $this->secret,
        ], $result, 'Expect to get all named variables.');
    }

    public function testWithContainer()
    {
        $container1 = new Container();
        $container1->set(StringHolder::class, new Dummy1('dummy1_' . $this->secret));
        $container2 = new Container();
        $container2->set(StringHolder::class, new Dummy2('dummy2_' . $this->secret));

        $caller = (new Caller($container1))->withContainer($container2);
        $str = $caller->call(function (StringHolder $s) {
            return (string) $s;
        }, []);
        $this->assertEquals($str, 'dummy2_' . $this->secret);
    }

    public function testCallArrayStyle()
    {
        $container = new Container();
        $caller = new Caller($container);
        $s = new StringHolder('callable_class_' . $this->secret);
        $msg = $caller->call([$s, 'getMessage'], ['foo' => 'bar']);
        $this->assertEquals('callable_class_' . $this->secret, $msg);
    }

    public function testCallFunctionName()
    {
        $container = new Container();
        $container->set(Dummy1::class, new Dummy1('dummy1_' . $this->secret));
        $caller = new Caller($container);

        /**
         * @var \Phata\HttpCore\Test\Dummy1
         */
        $dummy = $caller->call(getDummy::class, ['foo' => 'bar']);
        $this->assertEquals('dummy1_' . $this->secret, $dummy->getMessage());
    }

    public function testCallClousure()
    {
        $container = new Container();
        $container->set(Dummy1::class, new Dummy1('dummy1_' . $this->secret));
        $caller = new Caller($container);

        /**
         * @var \Phata\HttpCore\Test\Dummy1
         */
        $dummy = $caller->call(function (Dummy1 $dummy) {
            return $dummy;
        }, ['foo' => 'bar']);
        $this->assertEquals('dummy1_' . $this->secret, $dummy->getMessage());
    }

}