<?php

namespace Phata\HttpCore\Container\Test;

use DI\Container;
use Phata\HttpCore\Container\OverrideContainer;

class OverrideContainerTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests getter methods
    public function testOverriding()
    {
        $fallback = new Container();
        $fallback->set('hello', 'world');
        $fallback->set('foo', 'bar');
        $fallback->set('good', 'day');
        $container = (new OverrideContainer($fallback, [
            'hello' => 'world:initialized',
        ]))->with('foo', 'bar:methodCall');

        $this->assertEquals(
            true,
            $container->has('hello'),
        );
        $this->assertEquals(
            true,
            $container->has('foo'),
        );
        $this->assertEquals(
            true,
            $container->has('good'),
        );
        $this->assertEquals(
            false,
            $container->has('bad'),
        );

        $this->assertEquals(
            'world:initialized',
            $container->get('hello'),
        );
        $this->assertEquals(
            'bar:methodCall',
            $container->get('foo'),
        );
        $this->assertEquals(
            'day',
            $container->get('good'),
        );
    }
}