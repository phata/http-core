<?php

namespace Phata\HttpCore\Container\Test;

use Http\Factory\Discovery\HttpFactory;
use Phata\HttpCore\Message\MultipartStream;

class MultipartStreamTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /** @specify */

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var \Psr\Http\Message\StreamFactoryInterface
     */
    protected $factory;

    protected function _before()
    {
        $this->secret = rand(0, 100000);
        $this->factory = HttpFactory::streamFactory();
    }

    protected function _after()
    {
    }

    protected static function createTempResource($contents)
    {
        $fh = fopen('php://temp', 'r+');
        fwrite($fh, $contents);
        fseek($fh, 0);
        return $fh;
    }

    // test basic reading ability
    public function testBasicReading()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('<html>hello stream 2</html>')),
                ['Content-Type: text/html'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 3')),
            );

        $this->assertEquals(
            str_replace("\r\n\n", "\r\n", <<<TEXT
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/plain\r\n
                \r\n
                hello stream 1\r\n
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/html\r\n
                \r\n
                <html>hello stream 2</html>\r\n
                --{$multipart->getBoundary()}\r\n
                \r\n
                hello stream 3\r\n
                --{$multipart->getBoundary()}--\r\n
                TEXT),
            $multipart->read(8192),
        );
    }

    // test basic reading ability
    public function testGetContents()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('<html>hello stream 2</html>')),
                ['Content-Type: text/html'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 3')),
            );

        $this->assertEquals(
            str_replace("\r\n\n", "\r\n", <<<TEXT
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/plain\r\n
                \r\n
                hello stream 1\r\n
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/html\r\n
                \r\n
                <html>hello stream 2</html>\r\n
                --{$multipart->getBoundary()}\r\n
                \r\n
                hello stream 3\r\n
                --{$multipart->getBoundary()}--\r\n
                TEXT),
            $multipart->getContents(),
        );
    }

    // test basic reading ability
    public function testToString()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('<html>hello stream 2</html>')),
                ['Content-Type: text/html'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 3')),
            );

        $this->assertEquals(
            str_replace("\r\n\n", "\r\n", <<<TEXT
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/plain\r\n
                \r\n
                hello stream 1\r\n
                --{$multipart->getBoundary()}\r\n
                Content-Type: text/html\r\n
                \r\n
                <html>hello stream 2</html>\r\n
                --{$multipart->getBoundary()}\r\n
                \r\n
                hello stream 3\r\n
                --{$multipart->getBoundary()}--\r\n
                TEXT),
            (string) $multipart,
        );
    }

    /**
     * @depends testBasicReading
     */
    public function testReadPartial()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 2')),
                ['Content-Type: text/html'],
            );

        $expected_full_render = str_replace("\r\n\n", "\r\n", <<<TEXT
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/plain\r\n
            \r\n
            hello stream 1\r\n
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/html\r\n
            \r\n
            hello stream 2\r\n
            --{$multipart->getBoundary()}--\r\n
            TEXT);

        $cutoff = 140;
        $part1 = $multipart->read($cutoff);
        $part2 = $multipart->read(1024);

        $this->assertEquals(
            substr($expected_full_render, 0, $cutoff),
            $part1,
        );
        $this->assertEquals(
            substr($expected_full_render, $cutoff),
            $part2,
        );
        $this->assertEquals(
            $expected_full_render,
            $part1 . $part2,
        );
    }

    public function testMethodClose()
    {
        $fh1 = static::createTempResource('hello stream 1');
        $fh2 = static::createTempResource('hello stream 2');

        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource($fh1),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource($fh2),
                ['Content-Type: text/plain'],
            );

        // underlying resources are still valid
        $this->assertEquals(true, is_resource($fh1));
        $this->assertEquals(true, is_resource($fh2));

        $multipart->close();

        // underlying resources should be closed
        $this->assertEquals(false, is_resource($fh1));
        $this->assertEquals(false, is_resource($fh2));
    }

    public function testMethodGetSize()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 2')),
                ['Content-Type: text/plain'],
            );

        $expected_full_render = str_replace("\r\n\n", "\r\n", <<<TEXT
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/plain\r\n
            \r\n
            hello stream 1\r\n
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/plain\r\n
            \r\n
            hello stream 2\r\n
            --{$multipart->getBoundary()}--\r\n
            TEXT);

        $this->assertEquals(
            strlen($expected_full_render),
            $multipart->getSize(),
        );
    }

    /**
     * @depends testReadPartial
     */
    public function testMethodEof()
    {
        // Create a multipart stream to test
        $multipart = (new MultipartStream())
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 1')),
                ['Content-Type: text/plain'],
            )
            ->add(
                $this->factory->createStreamFromResource(
                    static::createTempResource('hello stream 2')),
                ['Content-Type: text/plain'],
            );

        $expected_full_render = str_replace("\r\n\n", "\r\n", <<<TEXT
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/plain\r\n
            \r\n
            hello stream 1\r\n
            --{$multipart->getBoundary()}\r\n
            Content-Type: text/plain\r\n
            \r\n
            hello stream 2\r\n
            --{$multipart->getBoundary()}--\r\n
            TEXT);

        $second_boundary_start_pos = strpos(
            $expected_full_render,
            "--{$multipart->getBoundary()}\r\n",
            10,
        );
        $end_boundary_start_pos = strpos(
            $expected_full_render,
            "--{$multipart->getBoundary()}--\r\n",
            $second_boundary_start_pos,
        );

        $this->assertEquals(false, $multipart->eof(),
            'Just started. MultipartStream::eof should return false');

        // Read until reaching the beginning of stream 2
        // headers. The reading to second stream is not started
        // yet. So the entire multipart stream should not
        // have been reach eof
        $multipart->read($second_boundary_start_pos);
        $this->assertEquals(false, $multipart->eof(),
            'First stream is done. MultipartStream::eof should return false');

        // Read until arrive at the end boundary of stream 2.
        // All the internal stream has reached their eof.
        // But the multipart stream has not ended yet.
        $multipart->read($end_boundary_start_pos - $second_boundary_start_pos - 1);
        $this->assertEquals(false, $multipart->eof(),
            'Reached second stream end boundary. MultipartStream::eof should return false');

        // Read the remaining content.
        // Reached eof now.
        $multipart->read(1024);
        $this->assertEquals(true, $multipart->eof(),
            'All contents are read. MultipartStream::eof should return true');
    }
}