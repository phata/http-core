<?php

namespace Phata\HttpCore\Middleware\Test;

use Http\Factory\Discovery\HttpFactory;
use Phata\HttpCore\Middleware\Chain;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class AppendRequestHeader implements MiddlewareInterface
{
    /**
     * @var string
     */
    var $name;

    /**
     * @var string
     */
    var $value;

    public function __construct(string $header_name, string $header_value)
    {
        $this->name = $header_name;
        $this->value = $header_value;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request->withAddedHeader($this->name, $this->value));
    }
}

class AppendResponseHeader implements MiddlewareInterface
{
    /**
     * @var string
     */
    var $name;

    /**
     * @var string
     */
    var $value;

    public function __construct(string $header_name, string $header_value)
    {
        $this->name = $header_name;
        $this->value = $header_value;
    }

    /**
     * {@inheritDoc}
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        return $response->withAddedHeader($this->name, $this->value);
    }
}

class ShowHeaderHandler implements RequestHandlerInterface
{
    function handle(ServerRequestInterface $request): ResponseInterface
    {
        $streamFactory = HttpFactory::streamFactory();
        $responseFactory = HttpFactory::responseFactory();
        $body = $streamFactory->createStream(json_encode([
            'request_header' => $request->getHeaders(),
        ]));
        return $responseFactory->createResponse()->withBody($body);
    }
}

class ChainTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests getter methods
    public function testChain()
    {
        $chain = new Chain(
            new AppendRequestHeader('X-Added-Header-Foo', 'request:foo'),
            new AppendRequestHeader('X-Added-Header-Bar', 'request:bar'),
            new AppendRequestHeader('X-Added-Header-Hello', 'request:hello'),
            new AppendResponseHeader('X-Added-Header-Foo', 'response:foo'),
            new AppendResponseHeader('X-Added-Header-Bar', 'response:bar'),
            new AppendResponseHeader('X-Added-Header-Hello', 'response:hello'),
        );
        $request = HttpFactory::serverRequestFactory()
            ->createServerRequest('GET', '/hello/world');
        $handler = new ShowHeaderHandler();
        $response = $chain->process($request, $handler);

        // Check the request (by inspecting the response body JSON)
        // Note: expecting to be first in first out
        $request_received = json_decode((string) $response->getBody(), true);
        $this->assertSame([
            'X-Added-Header-Foo' => ['request:foo'],
            'X-Added-Header-Bar' => ['request:bar'],
            'X-Added-Header-Hello' => ['request:hello'],
        ], $request_received['request_header']);

        // Check the response
        // Note: expecting to be last in first out
        $this->assertSame([
            'X-Added-Header-Hello' => ['response:hello'],
            'X-Added-Header-Bar' => ['response:bar'],
            'X-Added-Header-Foo' => ['response:foo'],
        ], $response->getHeaders());
    }
}