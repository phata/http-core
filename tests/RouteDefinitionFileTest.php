<?php

namespace Phata\HttpCore\Test;

use Phata\HttpCore\RouteDefinitionFile;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std as RouteParser;
use FastRoute\DataGenerator\GroupPosBased as DataGenerator;

class RouteDefinitionFileTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /** @specify */
    protected $tmpfname;
    protected $secret;

    protected function _before()
    {
        $this->secret = rand(0, 100000);
        $this->tmpfname = tempnam(sys_get_temp_dir(), 'phata-httpcore--route-definition-file-test');
        $handle = fopen($this->tmpfname, 'w');
        fwrite($handle, <<<TXT
<?php

// Dummy route with a dummy function name
\$r->addRoute('GET', '/', 'hello_{$this->secret}');
TXT
);
        fclose($handle);
    }

    protected function _after()
    {
    }

    // tests
    public function testBasic()
    {
        $r = new RouteCollector(new RouteParser(), new DataGenerator());
        $def = new RouteDefinitionFile($this->tmpfname);
        $def->collectRoutes($r);
        $data = $r->getData();

        $this->assertEquals($data[0], [
            'GET' => [
                '/' => 'hello_' . $this->secret,
            ],
        ]);
    }

    public function testNonExistsFile()
    {
        try {
            new RouteDefinitionFile($this->tmpfname . '-' . $this->secret . '-non-exists-file.txt');
        } catch (\Exception $e) {
            $exception = $e;
        }

        $this->assertInstanceOf(\InvalidArgumentException::class, $exception ?? null,
            'Expecting an InvalidArgumentException for initializing RouteDefinitionFile with non-exists file.');
    }
}