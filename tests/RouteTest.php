<?php

namespace Phata\HttpCore\Test;

use DI\Container;
use Phata\HttpCore\Route;

class DummyController1
{
    public function __construct($name)
    {
        $this->name = $name;
    }

    public function execute()
    {
        return static::class . '(' . $this->name . ')';
    }
}

class DummyController2
{
    public static function execute()
    {
        return static::class;
    }
}

class RouteTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /** @specify */
    protected $container;

    protected function _before()
    {
        $this->container = new Container;
        $this->container->set('dummy1', new DummyController1('my dummy1'));
        $this->container->set('dummy2', new DummyController2());
    }

    protected function _after()
    {
    }

    // tests getter methods
    public function testGetterMethods()
    {
        $id = 'dummyId:' . time();
        $methodName = 'dummyMethod:' . time();
        $route = new Route($id, $methodName);

        $this->specify('Route::getId should return originally assigned id', function() use ($route, $id) {
            $this->assertEquals($route->getId(), $id);
        });
        $this->specify('Route::getMethodName should return originally assigned method name', function() use ($route, $methodName) {
            $this->assertEquals($route->getMethodName(), $methodName);
        });
    }

    /**
     * @depends testGetterMethods
     */
    public function testCreateCallableWithDummy1InContainer()
    {
        $this->specify('Create callable identifing normal class method', function() {
            $route = new Route('dummy1', 'execute');
            $callable = $route->createCallable($this->container);
            $this->assertIsCallable($callable, '$callable should be Callable type');
            $this->assertIsArray($callable, '$callable should be an array');
            $this->assertEquals(2, sizeof($callable), '$callable should be an array of 2 items');
            $this->assertInstanceOf(DummyController1::class, $callable[0]);
            $this->assertEquals('execute', $callable[1], '$callable[1] should be the method name');
            $this->assertEquals(DummyController1::class . "(my dummy1)", $callable(), '$callable() should output proper name');
        });
    }

    /**
     * @depends testGetterMethods
     */
    public function testCreateCallableWithDummy2InContainer()
    {
        $this->specify('Create callable identifing static class method', function() {
            $route = new Route('dummy2', 'execute');
            $callable = $route->createCallable($this->container);
            $this->assertIsCallable($callable, '$callable should be Callable type');
            $this->assertIsArray($callable, '$callable should be an array');
            $this->assertEquals(2, sizeof($callable), '$callable should be an array of 2 items');
            $this->assertEquals(DummyController2::class, $callable[0]);
            $this->assertEquals('execute', $callable[1], '$callable[1] should be the method name');
            $this->assertEquals(DummyController2::class, $callable(), '$callable() should output proper name');
        });
    }
}