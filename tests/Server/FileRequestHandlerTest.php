<?php

namespace Phata\HttpCore\Server\Test;

use Http\Factory\Discovery\HttpFactory;
use Phata\HttpCore\Message\MultipartStream;
use Phata\HttpCore\Server\FileRequestHandler;

class FileRequestHandlerTest extends \Codeception\Test\Unit
{

    /**
     * @var \Psr\Http\Message\ResponseFactoryInterface
     */
    protected $responseFactory;

    /**
     * @var \Psr\Http\Message\ServerRequestFactoryInterface
     */
    protected $serverRequestFactory;

    /**
     * @var \Psr\Http\Message\StreamFactoryInterface
     */
    protected $streamFactory;

    protected function _before()
    {
        $this->responseFactory = HttpFactory::responseFactory();
        $this->serverRequestFactory = HttpFactory::serverRequestFactory();
        $this->streamFactory = HttpFactory::streamFactory();
    }

    protected function _after()
    {
    }

    public function testHandleRangeRequest_SingleRange()
    {
        $request = $this->serverRequestFactory
            ->createServerRequest('POST', '/dummy/uri')
            ->withAddedHeader('Content-Type', 'multipart/byteranges')
            ->withAddedHeader('Range', 'bytes=10-30');

        $tmpfile = tempnam(sys_get_temp_dir(), 'testHandleRangeRequest.tmp');
        file_put_contents($tmpfile, <<<FILECONTENTS
        0abcdefgh
        1abcdefgh
        2abcdefgh
        3abcdefgh
        4abcdefgh
        5abcdefgh
        6abcdefgh
        7abcdefgh
        8abcdefgh
        9abcdefgh\n
        FILECONTENTS);

        $handler = new FileRequestHandler(
            $this->streamFactory,
            $this->responseFactory,
            $tmpfile,
        );
        $response = $handler->handle($request);
        $this->assertEquals(
            '21',
            $response->getHeaderLine('Content-Length'),
        );
        $this->assertEquals(
            'bytes 10-30/100',
            $response->getHeaderLine('Content-Range'),
        );
        unlink($tmpfile);
    }

    /**
     * Note: According to the standard, all byte are named by their
     * offset to the first byte. For example
     *
     * Pos  0 1 2 3 4 5 6 7 8 9 10
     * Byte |A|B|C|D|E|F|G|H|I|J|
     *
     * If a range request specify "bytes=3-7", it means byte #3
     * to byte #7 (includes #7). That would be DEFGH. The length
     * would be (1 + 7 - 3) = 5. A bit counter intuitive.
     */
    public function testHandleRangeRequest_MultipleRanges()
    {
        $request = $this->serverRequestFactory
            ->createServerRequest('POST', '/dummy/uri')
            ->withAddedHeader('Content-Type', 'multipart/byteranges')
            ->withAddedHeader('Range', 'bytes=-9, 15-29, 45-59, 75-');

        $tmpfile = tempnam(sys_get_temp_dir(), 'testHandleRangeRequest.tmp');
        file_put_contents($tmpfile, <<<FILECONTENTS
        0abcdefgh
        1abcdefgh
        2abcdefgh
        3abcdefgh
        4abcdefgh
        5abcdefgh
        6abcdefgh
        7abcdefgh
        8abcdefgh
        9abcdefgh\n
        FILECONTENTS);

        $handler = new FileRequestHandler(
            $this->streamFactory,
            $this->responseFactory,
            $tmpfile,
        );

        // check internal content type
        $contentType = FileRequestHandler::getMimeContentType($tmpfile);
	    $this->assertNotFalse(
            preg_match(
                '~^text/plain~',
                $contentType
            )
        );

        // get response to test
        $response = $handler->handle($request);
        $this->assertNotFalse(
            preg_match(
                '~^multipart/byteranges; boundary=(?<boundary>.+?)$~',
                $response->getHeaderLine('Content-Type'),
                $matches
            ),
            'Content type is multipart/byterange; boundary=some-boundary-string',
        );

        /**
         * @var \Phata\HttpCore\Message\MultipartStream
         */
        $body = $response->getBody();
        $this->assertInstanceOf(MultipartStream::class, $body,
            'Body for multipart byterange request should return multipart stream as body.');

        $expected = str_replace("\r\n\n", "\r\n", <<<HEREDOC
        --{$matches['boundary']}\r\n
        Content-Type: {$contentType}\r\n
        Content-Length: 10\r\n
        Content-Range: bytes 0-9/100\r\n
        \r\n
        0abcdefgh
        \r\n
        --{$matches['boundary']}\r\n
        Content-Type: {$contentType}\r\n
        Content-Length: 15\r\n
        Content-Range: bytes 15-29/100\r\n
        \r\n
        efgh
        2abcdefgh
        \r\n
        --{$matches['boundary']}\r\n
        Content-Type: {$contentType}\r\n
        Content-Length: 15\r\n
        Content-Range: bytes 45-59/100\r\n
        \r\n
        efgh
        5abcdefgh
        \r\n
        --{$matches['boundary']}\r\n
        Content-Type: {$contentType}\r\n
        Content-Length: 25\r\n
        Content-Range: bytes 75-99/100\r\n
        \r\n
        efgh
        8abcdefgh
        9abcdefgh
        \r\n
        --{$matches['boundary']}--\r\n
        HEREDOC);

        $this->assertEquals($expected, $body->getContents());
        unlink($tmpfile);
    }
}
