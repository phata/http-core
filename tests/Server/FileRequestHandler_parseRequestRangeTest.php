<?php

namespace Phata\HttpCore\Server\Test;

use Phata\HttpCore\Server\FileRequestHandler;

class FileRequestHandler_parseRequestRangeTest extends \Codeception\Test\Unit
{
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testParseSingleRange()
    {
        $this->assertEquals(
            [
                [
                    'unit' => 'bytes',
                    'start' => 5,
                    'end' => 11,
                    'size' => 7,
                    'total' => 1024,
                ],
            ],
            FileRequestHandler::parseRequestRange('bytes=5-11', 1024),
            'Can parse single basic range 5-11'
        );

        $this->assertEquals(
            [
                [
                    'unit' => 'bytes',
                    'start' => 0,
                    'end' => 11,
                    'size' => 12,
                    'total' => 1024,
                ],
            ],
            FileRequestHandler::parseRequestRange('bytes=-11', 1024),
            'Can parse range omiting start -11'
        );

        $this->assertEquals(
            [
                [
                    'unit' => 'bytes',
                    'start' => 11,
                    'end' => 1023,
                    'size' => 1013,
                    'total' => 1024,
                ],
            ],
            FileRequestHandler::parseRequestRange('bytes=11-', 1024),
            'Can parse range omiting end 11-'
        );
    }

    /**
     * @depends testParseSingleRange
     */
    public function testParseMultipleRanges()
    {
        $this->assertEquals(
            [
                [
                    'unit' => 'bytes',
                    'start' => 5,
                    'end' => 11,
                    'size' => 7,
                    'total' => 1024,
                ],
                [
                    'unit' => 'bytes',
                    'start' => 14,
                    'end' => 17,
                    'size' => 4,
                    'total' => 1024,
                ],
                [
                    'unit' => 'bytes',
                    'start' => 20,
                    'end' => 24,
                    'size' => 5,
                    'total' => 1024,
                ],
            ],
            FileRequestHandler::parseRequestRange('bytes=5-11, 14-17, 20-24', 1024),
            'Can parse multiple basic range',
        );

        $this->assertEquals(
            [
                [
                    'unit' => 'bytes',
                    'start' => 0,
                    'end' => 11,
                    'size' => 12,
                    'total' => 24,
                ],
                [
                    'unit' => 'bytes',
                    'start' => 14,
                    'end' => 17,
                    'size' => 4,
                    'total' => 24,
                ],
                [
                    'unit' => 'bytes',
                    'start' => 20,
                    'end' => 23,
                    'size' => 4,
                    'total' => 24,
                ],
            ],
            FileRequestHandler::parseRequestRange('bytes=-11, 14-17, 20-', 24),
            'Can parse multiple mixed range',
        );
    }
}