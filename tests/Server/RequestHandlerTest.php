<?php

namespace Phata\HttpCore\Server\Test;

use DI\Container;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Http\Factory\Discovery\HttpFactory;
use Phata\HttpCore\Container\CallerInterface;
use Phata\HttpCore\Container\Caller;
use Phata\HttpCore\Server\RequestHandler;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class RequestHandlerTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /** @specify */
    protected $container;

    /**
     * @var \Psr\Http\Message\ServerRequestFactoryInterface
     */
    protected $serverRequestFactory;

    protected function _before()
    {
        $this->serverRequestFactory = HttpFactory::serverRequestFactory();
    }

    protected function _after()
    {
    }

    // tests
    public function testHandlingRequest()
    {
        $container = new Container();
        $secret = rand(0, 100000);

        // Setup router dispatcher
        $container->set(Dispatcher::class, \DI\factory(function () use ($secret) {
            return \FastRoute\simpleDispatcher(function (RouteCollector $r) use ($secret) {
                $r->get('/dummy-path', function (ServerRequestInterface $request) use ($secret) {
                    return json_encode([
                        'header' => $request->getHeaderLine('X-Added-Header'),
                        'secret' => $secret,
                    ]);
                });
            });
        }));

        // Associate the Caller to CallerInterface
        $container->set(CallerInterface::class, \DI\get(Caller::class));

        // Provides factories
        $container->set(ResponseFactoryInterface::class, \DI\factory([HttpFactory::class, 'responseFactory']));
        $container->set(StreamFactoryInterface::class, \DI\factory([HttpFactory::class, 'streamFactory']));

        // create request handler
        $handler = new RequestHandler($container);

        // Create dummy request to "/dummy-path"
        /**
         * @var \Psr\Http\Message\ServerRequestInterface
         */
        $request = $this->serverRequestFactory
            ->createServerRequest('GET', '/dummy-path')
            ->withAddedHeader('X-Added-Header', 'added-' . $secret);

        // assert response body is $secret
        $response = $handler->handle($request);
        $response_vars = json_decode((string) $response->getBody());

        // check if the response body (trimmed) is the secret number.
        $this->assertEquals(
            (string) $secret,
            $response_vars->secret,
        );

        $this->assertEquals(
            (string) 'added-' . $secret,
            $response_vars->header,
        );
    }
}